package be.kdg.programming3.calculatordemo;

public interface CalculatorService {
    int sum(int a, int b);
}
