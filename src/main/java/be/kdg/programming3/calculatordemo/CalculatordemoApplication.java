package be.kdg.programming3.calculatordemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CalculatordemoApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(CalculatordemoApplication.class, args);
        CalculatorService cs = context.getBean(CalculatorService.class);
        int result = cs.sum(10, 20);
        System.out.println("The result:" + result);
        CalculatorService cs2 = context.getBean(CalculatorService.class);
        System.out.println("First bean: " + cs);
        System.out.println("Second bean: " + cs2);
    }

   /* @Bean
    public CalculatorService calculatorService(){
        return new GoodCalculatorService();
    }*/

}
