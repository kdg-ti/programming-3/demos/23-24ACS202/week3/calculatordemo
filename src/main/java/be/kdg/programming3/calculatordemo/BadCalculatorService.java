package be.kdg.programming3.calculatordemo;

import org.springframework.stereotype.Component;

@Component
public class BadCalculatorService implements CalculatorService{
    @Override
    public int sum(int a, int b) {
        System.out.println("Forget it!");
        return 0;
    }
}
