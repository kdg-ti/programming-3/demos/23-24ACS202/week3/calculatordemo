package be.kdg.programming3.calculatordemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CalculatorServiceUser {
    private CalculatorService calculatorService;


    public CalculatorServiceUser(@Qualifier("goodCalculatorService") CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public void useTheCalculator(){
        System.out.println("Doing a sum!");
        int result = calculatorService.sum(1, 2);
        System.out.println("Found result:" + result);
    }
}
