package be.kdg.programming3.calculatordemo;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Primary
//@Scope("prototype")
public class GoodCalculatorService implements CalculatorService{
    @Override
    public int sum(int a, int b) {
        System.out.println("Doing the sum!");
        return a + b;
    }
}
